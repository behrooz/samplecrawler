# -*- coding: utf-8 -*-
import scrapy
from mehrdad.items import MehrdadItem

class UnlSpider(scrapy.Spider):
    name = "unl"
    allowed_domains = ["unl.edu"]
    start_urls = (
        'http://www.unl.edu/',
    )

    def parse(self, response):
        item = MehrdadItem()
        item['title'] = response.xpath('//title/text()').extract()[0]
        yield item
        pass
